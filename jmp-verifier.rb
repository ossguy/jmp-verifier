#!/usr/bin/env ruby
#
# Copyright (C) 2017  Denver Gingerich <denver@ossguy.com>
# Copyright (C) 2017  Stephen Paul Weber <singpolyma@singpolyma.net>
#
# This file is part of jmp-verifier.
#
# jmp-verifier is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# jmp-verifier is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with jmp-verifier.  If not, see <http://www.gnu.org/licenses/>.

$stdout.sync = true

puts "JMP Verifier - does user control phone number?\n"\
	"source code: https://gitlab.com/ossguy/jmp-verifier\n"\
	"==>> last commit of this version is " + `git rev-parse HEAD` + "\n"

require 'em-hiredis'
require 'em-http-request'
require 'goliath'
require 'json'
require 'securerandom'
require 'set'
require 'uri'

require_relative 'em_promise'

t = Time.now
puts "LOG %d.%09d: starting...\n\n" % [t.to_i, t.nsec]

# read in the settings file, trimming the first and last lines
eval File.readlines('../../../../jvr_settings.rb')[1..-2].join("\n")

$calls = SortedSet.new()

EM.next_tick do
	REDIS = EM::Hiredis.connect

	EM.add_periodic_timer(0.6) do
		WebhookHandler.check_and_call()
	end
end

class Call
	include Comparable

	attr_reader :time
	attr_reader :logger
	attr_reader :number
	attr_reader :api_key
	attr_reader :brand
	attr_reader :request_id

	def initialize(time, logger, number, api_key, brand, request_id)
		@time = time
		@logger = logger
		@number = number
		@api_key = api_key
		@brand = brand
		@request_id = request_id
	end

	def <=>(another_call)
		if self.time < another_call.time
			-1
		elsif self.time > another_call.time
			1
		else
			0
		end
	end
end

class WebhookHandler < Goliath::API
	use Goliath::Rack::Params

	def send_sms(logger, number, brand, code)
		EM::HttpRequest.new(
			"https://api.plivo.com/v1/Account/#{$user_id}/Message/"
		).post(
			head: {
				'Authorization'	=> [$user_id, $token],
				'Content-Type'	=> 'application/json'
			},
			body: JSON.dump({
				'src'	=> $from_num,
				'dst'	=> number,
				'text'	=> "Your #{brand} verify code: #{code}",
				'url'	=> $cb_url
			})
		).then { |http|
			logger.debug "sAPI response code to send: " +
				http.response_header.status.to_s

			case http.response_header.status
			when 202
				0	# status: Success
			else
				EMPromise.reject(http.response_header.status)
			end
		}
	end

	def self.make_call(call)
		EM::HttpRequest.new(
			"https://api.plivo.com/v1/Account/#{$user_id}/Call/"
		).post(
			head: {
				'Authorization'	=> [$user_id, $token],
				'Content-Type'	=> 'application/json'
			},
			body: JSON.dump({
				'from'		=> $from_num,
				'to'		=> call.number,
				'answer_url'	=> $url_base + '/call_cb-' +
					$call_key + '-' + call.api_key + '-' +
					call.brand
			})
		).then { |http|
			call.logger.debug "cAPI response code to send: " +
				http.response_header.status.to_s

			#return EMPromise.reject(http.response_header.status) if
			#	http.response_header.status != 202

			case http.response_header.status
			when 201
				0	# status: Success
			else
				EMPromise.reject(http.response_header.status)
			end
		}
	end

	def self.check_and_call()
		return if $calls.empty?

		next_call = $calls.to_a.first
		t = Time.now

		if next_call.time < t.to_i
			# delete first or may double-call if make_call too long
			$calls.delete(next_call)
			make_call(next_call)

			# we're done - if there are more we get on next tick; we
			#  can't do them now because that'd violate Plivo policy
		end
	end

	def response(env)
		env.logger.debug 'ENV: ' + env.to_s
		env.logger.debug 'path: ' + env['REQUEST_PATH']
		env.logger.debug 'params: ' + env.params.to_s
		env.logger.debug 'method: ' + env['REQUEST_METHOD']
		env.logger.debug 'BODY: ' + Rack::Request.new(env).body.read

		# TODO: figure out good API version strategy - start: /v1/verify

		if [params['api_key'], params['api_secret'] ] !=
			[$api_key, $api_secret] and not
			env['REQUEST_PATH'].start_with?('/call_cb-' + $call_key)

			return [
				401,
				{'Content-Type' => 'text/plain;charset=utf-8'},
				"invalid api_key or api_secret"
			]
		end

		if env['REQUEST_PATH'] == '/'
			if not params.key? 'number' or params['number'].empty?
				return [
					403,
					{'Content-Type' =>
						'text/plain;charset=utf-8'},
					"no number specified"
				]
			elsif not params.key? 'brand' or params['brand'].empty?
				return [
					403,
					{'Content-Type' =>
						'text/plain;charset=utf-8'},
					"no brand specified"
				]
			end

			request_id = env.params['api_key'] + "-" +
				env.params['number']

			# we use a 6-digit code as a security/UX compromise
			code = "%06d" % SecureRandom.random_number(1000000)

			code_key = "jvr_code-" + params['api_key'] + "-" +
				params['number']

			REDIS.setnx(code_key, code).then { |dne|
				if dne == 1
					# number did not exist - set expiry time
					REDIS.expire(code_key, $code_ttl
						).then { |rv|

						if rv != 1
							EMPromise.reject(rv)
						end
					}
				elsif dne == 0
					# "Concurrent verifications to the same
					#   number are not allowed"
					EMPromise.reject(409)
				else
					EMPromise.reject('setnx: ' + dne.to_s)
				end
			}.then {
				t_now = Time.now.to_i

				$calls << Call.new(t_now + $call1_timeout,
					env.logger,
					env.params['number'], $api_key,
					env.params['brand'], request_id)

				$calls << Call.new(t_now + $call1_timeout +
					$call2_timeout,
					env.logger,
					env.params['number'], $api_key,
					env.params['brand'], request_id)

				send_sms(
					env.logger,
					env.params['number'],
					env.params['brand'],
					code
				)
			}.then { |status|
				[
					200,
					{'Content-Type' => 'application/json'},
					JSON.dump({
						'request_id'	=> request_id,
						'status'	=> status,
						'error_text'	=> ""  # TODO
					})
				]
			}.catch { |rc|
				if rc.is_a?(Integer)
					EMPromise.reject(rc)
				else
					env.logger.error("ERROR: #{rc.inspect}")
					EMPromise.reject(500)
				end
			}.catch { |code|
				result = {
					'request_id'	=>
						env.params['api_key'] +
						"-" +
						env.params['number']
				}

				case code
				when 409
					result['status'] = 10
					result['error_text'] = 'Concurrent ' +
						'verifications to the same ' +
						'number (ie. within ' +
						$code_ttl.to_s + ' seconds ' +
						'of each other) are not allowed'
				else
					result['status'] = 5
					result['error_text'] =
						'unexpected error: ' + code.to_s
				end

				[
					code,
					{'Content-Type' => 'application/json'},
					JSON.dump(result)
				]
			}.sync
		elsif env['REQUEST_PATH'] == '/check'
			if not params.key? 'request_id' or
				params['request_id'].empty?

				return [
					403,
					{'Content-Type' =>
						'text/plain;charset=utf-8'},
					"no request_id specified"
				]
			elsif not params.key? 'code' or params['code'].empty?
				return [
					403,
					{'Content-Type' =>
						'text/plain;charset=utf-8'},
					"no code specified"
				]
			end

			code_key = "jvr_code-" + params['request_id']

			REDIS.get(code_key).then { |code|
				if code.nil?
					[
						200,
						{'Content-Type' =>
							'application/json'},
						JSON.dump({
						'event_id' =>
							env.params[
								'request_id'],
						'status' => 101,
						'error_text' => "DNE or expired"
						})
					]
				elsif code == env.params['code']
					# match found so don't bother user again
					$calls.delete_if { |call|
						call.request_id ==
							env.params['request_id']
					}

					[
						200,
						{'Content-Type' =>
							'application/json'},
						JSON.dump({
						'event_id' =>
							env.params[
								'request_id'],
						'status' => 0,
						'error_text' => ""
						})
					]
				else
					[
						200,
						{'Content-Type' =>
							'application/json'},
						JSON.dump({
						'event_id' =>
							env.params[
								'request_id'],
						'status' => 16,
						'error_text' => "does not match"
						})
					]
				end
			}.catch { |rc|
				[
					500,
					{'Content-Type' => 'application/json'},
					JSON.dump({
						'event_id' =>
							env.params[
								'request_id'],
						'status' => 5,
						'error_text' =>
							'unexpected error: ' +
							rc.to_s
					})
				]
			}.sync
		elsif env['REQUEST_PATH'].start_with?('/call_cb-' + $call_key)
			api_key, brand = env['REQUEST_PATH'].split('-', 4)[2..3]

			code_key = "jvr_code-#{api_key}-#{params['To']}"

			REDIS.get(code_key).then { |code|
				if code.nil?
					[
						200,
						{'Content-Type' =>
							'application/xml'},
						'<?xml version="1.0"
encoding="UTF-8" ?>
<Response>
	<Speak loop="3">Your ' + brand +
' verification code has expired.  Please try again.</Speak>
</Response>'
					]
				else
					[
						200,
						{'Content-Type' =>
							'application/xml'},
						'<?xml version="1.0"
encoding="UTF-8" ?>
<Response>
	<Speak loop="5">Your ' + brand +
' verification code is ' + code.gsub(/([0-9])/, '\1, ') + '</Speak>
</Response>'
					]
				end
			}.catch { |code|
				[
					200,
					{'Content-Type' => 'application/xml'},
					'<?xml version="1.0" encoding="UTF-8" ?>
<Response>
	<Speak loop="3">Your ' + brand +
' code is unavailable.  Please try again.</Speak>
</Response>'
				]
			}.sync
		else
			[
				404,
				{'Content-Type' => 'text/plain;charset=utf-8'},
				"unknown endpoint"
			]
		end
	end
end
